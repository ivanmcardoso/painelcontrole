import { Injectable } from '@angular/core';
import { Area } from './production-status/area/area';
import { Observable } from 'rxjs';
import { HttpErrorResponse, HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class APIReadService {

  private list_areas_url = 'http://localhost:8080/demo/all';

  constructor(private http: HttpClient) { }

  private areas: Area[];

  getAreas(): Observable<Area[]> {
    return this.http.get<Area[]>(this.list_areas_url);
  }

}
