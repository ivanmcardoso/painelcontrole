import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import { GridComponent } from './production-status/grid/grid.component';
import { LineComponent } from './production-status/line/line.component';
import { PhaseComponent } from './production-status/phase/phase.component';
import { StatusDirective } from './status.directive';
import { AreaComponent } from './production-status/area/area.component';
import { MaterialModule } from './production-status/material/material.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { HttpClientModule } from '@angular/common/http';
import { DialogComponent } from './production-status/phase/dialog/dialog.component';
import { CurrentTimeComponent } from './production-status/current-time/current-time.component';
import { CommandTowerComponent } from './production-status/command-tower/command-tower.component';
import { HeaderComponent } from './production-status/header/header.component';


@NgModule({
  declarations: [
    AppComponent,
    GridComponent,
    LineComponent,
    PhaseComponent,
    StatusDirective,
    AreaComponent,
    DialogComponent,
    CurrentTimeComponent,
    CommandTowerComponent,
    HeaderComponent,

  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModule,
    FlexLayoutModule,
    HttpClientModule
  ],
  entryComponents: [ DialogComponent, ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
