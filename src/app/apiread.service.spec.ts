import { TestBed } from '@angular/core/testing';

import { APIReadService } from './apiread.service';

describe('APIReadService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: APIReadService = TestBed.get(APIReadService);
    expect(service).toBeTruthy();
  });
});
