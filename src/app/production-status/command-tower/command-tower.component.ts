import { Component, OnInit, AfterContentInit } from '@angular/core';
import { APIReadService } from '../../apiread.service';
import { Area } from '../area/area';
import { interval, Observable, BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-command-tower',
  templateUrl: './command-tower.component.html',
  styleUrls: ['./command-tower.component.css']
})
export class CommandTowerComponent implements OnInit, AfterContentInit {
  
  public areas: Observable<Area[]>;
  areasSubject : BehaviorSubject<Area[]>;
  date : Date = null;
  
  constructor(private service: APIReadService) {
  }
  
  ngOnInit() {
    this.dataRead();
    this.areasSubject = new BehaviorSubject<Area[]>(null);
    this.areas = this.areasSubject.asObservable();
    
    interval(60000).subscribe(()=>this.dataRead());    
    
  }

  ngAfterContentInit(): void {
    this.dataRead();
  }
  
  dataRead(){
    this.service.getAreas()
      .subscribe(data =>{
        this.areasSubject.next(data);
        this.date = new Date();
        }, 
      error => {
   
      }
    );
  }

  refresh(){
    this.dataRead();
  }

}
