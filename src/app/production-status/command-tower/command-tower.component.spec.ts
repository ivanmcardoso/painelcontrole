import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommandTowerComponent } from './command-tower.component';

describe('CommandTowerComponent', () => {
  let component: CommandTowerComponent;
  let fixture: ComponentFixture<CommandTowerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommandTowerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommandTowerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
