import { Component, OnInit, Input } from '@angular/core';
import { Phase } from './phase';
import { MatDialog } from '@angular/material/dialog';
import { DialogComponent } from './dialog/dialog.component';

@Component({
  selector: 'app-phase',
  templateUrl: './phase.component.html',
  styleUrls: ['./phase.component.css']
})

export class PhaseComponent implements OnInit {

  @Input() phase: Phase;
  constructor(public dialog: MatDialog) { }

  ngOnInit() {
  }

  public popUp() {
    // tslint:disable-next-line: triple-equals
    if (this.phase.status != 'Ativo') {
      this.dialog.open(DialogComponent, {
        width: '250px',
        data: {erro: this.phase.erro}     
      });
    }
  }

}
