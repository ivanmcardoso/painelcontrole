import { Component, Input } from '@angular/core';
import { Phase } from '../phase/phase';


@Component({
  selector: 'app-line',
  templateUrl: './line.component.html',
  styleUrls: ['./line.component.css']
})

export class LineComponent {
  
  @Input() phases: Phase[];
  constructor() {
  }
}
