import { Phase } from "../phase/phase";

export class Line{
    id: number;
    lineNumber: number;
    phases: Phase[];
}