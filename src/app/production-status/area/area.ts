import { Line } from '../line/line';

export class Area {
    id: number;
    name: string;
    lines: Line[];
}
