
import { Component, OnInit, Input } from '@angular/core';
import { Line } from '../line/line';

@Component({
  selector: 'app-area',
  templateUrl: './area.component.html',
  styleUrls: ['./area.component.css']
})
export class AreaComponent implements OnInit {

  @Input() lines: Line[];

  constructor() { }

  ngOnInit() {
  }

}
