import { Component, OnInit, Input } from '@angular/core';
import { interval } from 'rxjs';

@Component({
  selector: 'app-current-time',
  templateUrl: './current-time.component.html',
  styleUrls: ['./current-time.component.css']
})
export class CurrentTimeComponent implements OnInit {

  @Input() date: Date;
  currentTime: String;
  lastUpdate: string;

  constructor() { }

  ngOnInit() {

    this.setCurrentTime();
    this.setLastUpdate();
    interval(5000).subscribe(() => {
      this.setCurrentTime();
      this.setLastUpdate();
    })
  }

  setCurrentTime() {
    let currentDate = new Date();

    //Mount Current Time
    this.currentTime = (currentDate.getHours() < 10 ? '0'+currentDate.getHours() : currentDate.getHours()) + //Format Hours
      ':' + (currentDate.getMinutes() < 10 ? '0'+currentDate.getMinutes() : currentDate.getMinutes()) + //Format Monutes
      ' • ' + (currentDate.getDay() <10 ? '0'+currentDate.getDay() : currentDate.getDay()) + '/' + //Format Days
      (currentDate.getMonth() <10 ? '0'+currentDate.getMonth() : currentDate.getMonth() ) + '/' +//Format Mounth
      currentDate.getFullYear();
  }
  
  setLastUpdate() {
    if (this.date) {
      let currentDate = new Date();
      let millis = currentDate.getTime() - this.date.getTime();
      let seconds = Math.floor(millis / 1000);
      let minutes = Math.floor(seconds / 60);
      if (minutes < 1)
        this.lastUpdate = 'menos de 1 minuto.';
      else if (minutes == 1)
        this.lastUpdate = minutes + ' minuto.';
      else
        this.lastUpdate = minutes + ' minutos.';

      console.log('minutes', minutes);
    }
    else
      this.lastUpdate = 'menos de 1 minuto.';


  }

}
