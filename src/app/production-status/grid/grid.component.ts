import { Component, Input, OnInit } from '@angular/core';
import { Area } from '../area/area';
import { Observable } from 'rxjs';




@Component({
    // tslint:disable-next-line: component-selector
    selector: 'app-grid',
    templateUrl: './grid.component.html',
    styleUrls: ['./grid.component.css'],
})
export class GridComponent implements OnInit {

    @Input() areasObservable: Observable<Area[]>;
    areas: Area[];

    constructor() {
    }

    ngOnInit(): void {
        this.areasObservable.subscribe((areas: Area[]) => {
            this.areas = areas;
        });
    }
}
