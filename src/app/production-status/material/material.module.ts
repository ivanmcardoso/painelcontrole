import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {MatSelectModule} from '@angular/material/select';
import {MatDialogModule} from '@angular/material/dialog'; 
import {MatButtonModule} from '@angular/material/button'; 
import { MatIconRegistry } from "@angular/material/icon";
import { DomSanitizer } from "@angular/platform-browser";

import {
  MatGridListModule,
  MatIconModule,
  MatToolbarModule,
  MatCardModule,
  MatTooltipModule,

} from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  imports: [
    CommonModule,
    MatGridListModule,
    MatIconModule,
    MatSelectModule,
    MatToolbarModule,
    MatCardModule,
    MatTooltipModule,
    MatDialogModule,
    BrowserAnimationsModule,
    MatButtonModule,
  ],
  exports: [
    MatGridListModule,
    MatIconModule,
    MatSelectModule,
    MatToolbarModule,
    MatCardModule,
    MatTooltipModule,
    MatDialogModule,
    BrowserAnimationsModule,
    MatButtonModule,
  ],
  declarations: []
})
export class MaterialModule { 
  constructor(
    private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer
  ){
    this.matIconRegistry.addSvgIcon(
      "phase",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../assets/phase-ok.svg")
    );
  }

}
