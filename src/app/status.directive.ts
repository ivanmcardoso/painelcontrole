import { Directive, ElementRef, Input, AfterViewInit, OnChanges, SimpleChanges } from '@angular/core';

@Directive({
  selector: '[appStatus]'
})
export class StatusDirective implements OnChanges {
  static counter = 0;
  @Input() appStatus;

  styleEl: HTMLStyleElement = document.createElement('style');

  uniqueAttr = `app-status-${StatusDirective.counter++}`;

  constructor(private el: ElementRef) {
    const nativeEl: HTMLElement = this.el.nativeElement;
    nativeEl.setAttribute(this.uniqueAttr, '');
    nativeEl.appendChild(this.styleEl);
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.updateIcon();
  }

  updateIcon(): void {

    let color = 'lightgreen';
    let cursor = 'default';

    switch (this.appStatus) {
      case 'Ativo' : 
        color = '#17e794';
        cursor = 'default';
        break;
      case  'Inativo' :  
        color = 'lightgray'; 
        cursor = 'pointer';
        break;
      case  'Falha' :  
        color = 'lightcoral';
        cursor = 'pointer'; 
        break;
      case null : 
        color = 'white'; 
        cursor = 'default';
        break;
    }
    
    this.styleEl.innerText = `
      [${this.uniqueAttr}] {
        cursor: ${cursor};
        fill: ${color};
      }
    `;
  }
}
